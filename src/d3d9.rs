#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(non_upper_case_globals)]

use libc::*;
use windows as w;

pub type HRESULT = u32;

pub const D3D_SDK_VERSION : u32 = 32;
pub const D3D9b_SDK_VERSION : u32 = 31;

#[repr(C)]
pub struct IID {
    x : u64,
    s1 : u16,
    s2 : u16,
    c : [u8;8]
}

pub type REFIID = * const IID;
#[repr(C)]
pub enum D3DFORMAT {
    D3DFMT_UNKNOWN = 0,
    D3DFMT_R8G8B8 = 20,
    D3DFMT_A8R8G8B8 = 21,
    D3DFMT_X8R8G8B8 = 22
}

#[repr(C)]
pub struct D3DDISPLAYMODE
{
    pub Width: w::UINT,
    pub Height: w::UINT,
    pub RfreshRate : w::UINT,
    pub Format : D3DFORMAT,
}


pub type D3DSWAPEFFECT = CUInt;
pub type D3DMULTISAMPLE_TYPE = CUInt;

pub type FNPTR = *const CChar;

#[macro_export]
macro_rules! comcall {
    ($this:expr, $name:ident) => (
        unsafe{(((*(*$this).lpVtbl).$name))($this)}
    );
    ($this:expr, $name:ident,$($a:expr),*) => (
        unsafe{(((*(*$this).lpVtbl).$name))($this $(, $a)*)}
    )
}

#[repr(C)]
pub struct D3DPRESENT_PARAMETERS {
    BackBufferWidth : w::UINT,
    BackBufferHeight : w::UINT,
    BackBufferFormat : D3DFORMAT,
    MultiSampleType : D3DMULTISAMPLE_TYPE,
    MultiSampleQuality : w::DWORD,
    SwapEffect : D3DSWAPEFFECT,
    hDeviceWindow : w::HWND,
    Windowed : w::BOOL,
    EnableAutoDepthStencil : w::BOOL,
    AutoDepthStencilFormat : D3DFORMAT,
    Flags : w::DWORD,
    FullScreen_RefreshRateInHz : w::UINT,
    PresentationInterval : w::UINT
}

#[repr(C)]
pub struct IDirect3D9ExVtbl{
    pub QueryInterface : extern fn ( _ : * mut IDirect3D9Ex, REFIID ) -> HRESULT,
    pub AddRef : extern fn ( _ : * mut IDirect3D9Ex ),
    pub Release : extern fn ( _ : * mut IDirect3D9Ex ),

    pub RegisterSoftwareDevice : FNPTR,
    pub GetAdapterCount : extern fn (_ : *mut IDirect3D9Ex ) -> w::UINT,
    pub GetAdapterIdentifier : extern fn (_ : *mut IDirect3D9Ex, Adapter : w::UINT,
                                          Flags : w::DWORD, Format : D3DFORMAT ) -> HRESULT,
    pub GetAdapterModeCount : extern fn (_ : *mut IDirect3D9Ex, Adapter : w::UINT,
                                         Format : D3DFORMAT ) -> w::UINT,
    pub EnumAdapterModes : extern fn(_ : *mut IDirect3D9Ex, Adapter : w::UINT,
                                     Format : D3DFORMAT, Mode : w::UINT, pMode : *mut D3DDISPLAYMODE),
    pub GetAdapterDisplayMode : FNPTR,
    pub CheckDeviceType : FNPTR,
    pub CheckDeviceFormat : FNPTR,
    pub CheckDeviceMultiSampleType : FNPTR,
    pub CheckDepthStencilMatch : FNPTR,
    pub CheckDeviceFormatConversion : FNPTR,
    pub GetDeviceCaps : FNPTR,
    pub GetAdapterMonitor : FNPTR,
    pub CreateDevice : extern fn (_ : *mut IDirect3D9Ex, DevieType: w::UINT, hFocusWindow:w::HWND,
                                  BehaviorFlags : w::DWORD, pPresentationParameters : *mut D3DPRESENT_PARAMETERS,
                                  ppReturnedDeviceInterface : *mut LPDIRECT3D9EX),
}

#[repr(C)]
pub struct IDirect3DDevice9ExVtbl {
    TestCooperativeLevel : FNPTR,
    GetAdapterCount : FNPTR,
    EvictManagedResources : FNPTR,
    GetDirect3D : FNPTR,
    GetDeviceCaps : FNPTR,
    GetDisplayMode : FNPTR,
    GetCreationParameters : FNPTR,
    SetCursorProperties : FNPTR,
    SetCursorPosition : FNPTR,
    ShowCursor : FNPTR,
    CreateAdditionalSwapChain : FNPTR,
    GetSwapChain : FNPTR,
    GetNumberOfSwapChains : FNPTR,
    Reset : FNPTR,
    Present : FNPTR,
    GetBackBuffer : FNPTR,
    GetRasterStatus : FNPTR,
    SetDialogBoxMode : FNPTR,
    SetGammaRamp : FNPTR,
    GetGammaRamp : FNPTR,
    CreateTexture : FNPTR,
    CreateVolumeTexture : FNPTR,
    CreateCubeTexture : FNPTR,
    CreateVertexBuffer : FNPTR,
    CreateIndexBuffer : FNPTR,
    CreateRenderTarget : FNPTR,

    // ...
}
#[repr(C)]
pub struct IDirect3D9Ex {
    pub lpVtbl : *const IDirect3D9ExVtbl
}

pub type LPDIRECT3D9EX = * mut IDirect3D9Ex;

#[repr(C)]
pub struct IDirect3DDevice9Ex {
    pub lpVtbl : *const IDirect3DDevice9ExVtbl
}

pub type LPDIRECT3DDEVICE9EX = *mut IDirect3DDevice9Ex;

//#[link(name = "d3d9")]
//extern "stdcall" {
//    pub fn Direct3DCreate9Ex(SDKVersion : u32, ret : *mut*mut IDirect3D9Ex);
//}