#![allow(dead_code)]

pub type CInt = i32;
pub type CUInt = u32;
pub type CChar = i8;

extern {
    pub fn puts(_ : * const CChar) -> CInt;
}

#[macro_export]
macro_rules! cstr {
    ($e: expr) => ( transmute(($e).as_ptr()) )
}
