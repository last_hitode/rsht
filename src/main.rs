
mod windows;
#[macro_use]
mod d3d9;
#[macro_use]
mod libc;

const WIN_WIDTH : i32 = 200;
const WIN_HEIGHT : i32 = 200;

use windows as w;
use std::mem::transmute;

struct AppState {
    d3d : d3d9::LPDIRECT3D9EX,
    d3d_dev : d3d9::LPDIRECT3DDEVICE9EX
}

extern "stdcall" fn wnd_proc(hwnd: w::HWND,
                             msg : w::UINT,
                             wparam : w::WPARAM,
                             lparam : w::LPARAM) -> w::LRESULT
{
    match msg {
        w::WM_CLOSE => {
            unsafe{w::PostQuitMessage(0)};
            return 0;
        }

        w::WM_PAINT => {
            let mut ps : w::PAINTSTRUCT = unsafe{std::mem::uninitialized()};
            let dc = unsafe{w::BeginPaint(hwnd, &mut ps)};

            //let wbr = unsafe{w::GetStockObject(w::LTGRAY_BRUSH)};
            //let oldbr = unsafe{w::SelectObject(dc, wbr)};
            //unsafe{w::Rectangle(dc, 0, 0, WIN_WIDTH, WIN_HEIGHT)};

            let text = b"Hello, Unko!";
            unsafe{w::TextOutA(dc, 10, 100, transmute(text), text.len() as i32)};
            //unsafe{w::SelectObject(dc, oldbr)};
            unsafe{w::EndPaint(hwnd, &ps)};
            return 0;
        }

        w::WM_SIZE => {
            return 0;
        }

        _ => {
            return unsafe{w::DefWindowProcA(hwnd, msg, wparam, lparam)};
        }
    }
}

#[allow(while_true)]
fn main()
{
    let hinst = unsafe{w::GetModuleHandleA(std::ptr::null())};
    let mut cls : w::WNDCLASSEXA = unsafe{std::mem::uninitialized()};

    cls.cbSize = std::mem::size_of::<w::WNDCLASSEXA>() as u32;
    cls.style = w::CS_CLASSDC;
    cls.lpfnWndProc = wnd_proc;
    cls.cbClsExtra = 0;
    cls.cbWndExtra = 0;
    cls.hInstance = hinst;
    cls.hIcon = std::ptr::null_mut();
    cls.hCursor = unsafe{w::LoadCursorA(std::ptr::null_mut(),
                                        transmute(w::IDC_ARROW))};
    cls.hbrBackGround = std::ptr::null_mut();
    cls.lpszMenuName =std::ptr::null_mut();

    let clsname = b"aho\x00";
    cls.lpszClassName = unsafe{cstr!(clsname)};
    cls.hIconSm = std::ptr::null_mut();

    unsafe{w::RegisterClassExA(&cls)};
    let mut app : AppState = unsafe{std::mem::uninitialized()};

    let caption = b"aho\x00";
    let win = unsafe{w::CreateWindowExA(0,
                                        cstr!(clsname),
                                        cstr!(caption),
                                        //w::WS_OVERLAPPED| w::WS_MINIMIZEBOX | w::WS_SYSMENU,
                                        w::WS_OVERLAPPED_WINDOW,
                                        w::CW_USEDEFAULT,
                                        w::CW_USEDEFAULT,
                                        WIN_WIDTH, WIN_HEIGHT,
                                        std::ptr::null_mut(),
                                        std::ptr::null_mut(), hinst,
                                        transmute(&app))};

    unsafe{w::ShowWindow(win, w::SW_SHOW)};
    unsafe{w::UpdateWindow(win)};

    let mut d3d9 : d3d9::LPDIRECT3D9EX = std::ptr::null_mut();

    let d3d_dll = unsafe { w::LoadLibraryA(cstr!(b"d3d9.dll\x00")) };
    if d3d_dll == std::ptr::null_mut() {
        return;
    }

    let create : extern "stdcall" fn ( _:u32, _:*mut*mut d3d9::IDirect3D9Ex) = unsafe{
        transmute(w::GetProcAddress(d3d_dll, cstr!(b"Direct3DCreate9Ex\x00")))
    };

    create(d3d9::D3D_SDK_VERSION, &mut d3d9);

    if d3d9 == std::ptr::null_mut() {
        unsafe{w::MessageBoxA(std::ptr::null_mut(),
                              cstr!(b"fatal : create d3d failed"),
                              cstr!(b"error"),
                              w::MB_OK)};
        return;
    }

    app.d3d = d3d9;

    let num_adapter = comcall! (d3d9,GetAdapterCount);
    let num_mode = comcall! (d3d9,GetAdapterModeCount, 0, d3d9::D3DFORMAT::D3DFMT_X8R8G8B8);
    println!("num adapter = {a}, num mode = {b}", a=num_adapter, b=num_mode);

    {
        let mut modes = Vec::<d3d9::D3DDISPLAYMODE>::with_capacity(num_mode as usize);
        unsafe{modes.set_len(num_mode as usize)};

        for i in 0..num_mode {
            let m2 = modes.get_mut(i as usize);

            match m2 {
                Some(v) => {
                    comcall!(d3d9, EnumAdapterModes, 0, d3d9::D3DFORMAT::D3DFMT_X8R8G8B8, i, v);
                    println!("mode {n} : width={width}, height={height}",
                             n=i,
                             width=v.Width,
                             height=v.Height);

                },
                None => {},
            }

        }
    }

    while true {
        let mut msg : w::MSG = unsafe{std::mem::uninitialized()};
        let r = unsafe{w::GetMessageA(&mut msg, std::ptr::null_mut(), 0, 0)};

        if r == 0 || (r as i32) == -1 {
            break;
        }

        unsafe{w::TranslateMessage(&msg);}
        unsafe{w::DispatchMessageA(&msg);}
    }

    return;
}
