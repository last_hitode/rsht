#![allow(non_snake_case)]
#![allow(dead_code)]

use libc::{CUInt, CInt, CChar};

macro_rules! DEF_HANDLE {
    ($i:ident,$i2:ident) => (#[repr(C)] pub struct $i2 { unused : u8 }
                             pub type $i = *mut $i2;
                   )
}

macro_rules! cstr {
    ($e: expr) => ( transmute(($e).as_ptr()) )
}

DEF_HANDLE!(HWND, HWND__);
DEF_HANDLE!(HMODULE, HMODULE__);
//DEF_HANDLE!(HINSTANCE, HINSTANCE__);
pub type HINSTANCE = HMODULE;
DEF_HANDLE!(HICON, HICON__);
DEF_HANDLE!(HCURSOR, HCURSOR__);
DEF_HANDLE!(HMENU, HMENU__);
DEF_HANDLE!(HDC, HDC__);
DEF_HANDLE!(HGDIOBJ, HGDIOBJ__);

pub type HBRUSH = HGDIOBJ;

pub const MB_OK : CUInt = 0;

pub type IntPtr = u64;

pub type UINT = u32;
pub type LPCSTR = * const u8;
pub type LRESULT = IntPtr;
pub type WPARAM = IntPtr;
pub type LPARAM = IntPtr;
pub type DWORD = u32;
pub type LPVOID = * mut u8;
pub type BOOL = u32;
pub type LONG = u32;

pub type WNDPROC = extern "stdcall" fn (_:HWND, _:UINT, _:WPARAM, _:LPARAM) -> LRESULT;

pub const WS_OVERLAPPED      :u32  = 0x00000000;
pub const WS_POPUP           :u32  = 0x80000000;
pub const WS_CHILD           :u32  = 0x40000000;
pub const WS_MINIMIZE        :u32  = 0x20000000;
pub const WS_VISIBLE         :u32  = 0x10000000;
pub const WS_DISABLED        :u32  = 0x08000000;
pub const WS_CLIPSIBLINGS    :u32  = 0x04000000;
pub const WS_CLIPCHILDREN    :u32  = 0x02000000;
pub const WS_MAXIMIZE        :u32  = 0x01000000;
pub const WS_CAPTION         :u32  = 0x00C00000;
pub const WS_BORDER          :u32  = 0x00800000;
pub const WS_DLGFRAME        :u32  = 0x00400000;
pub const WS_VSCROLL         :u32  = 0x00200000;
pub const WS_HSCROLL         :u32  = 0x00100000;
pub const WS_SYSMENU         :u32  = 0x00080000;
pub const WS_THICKFRAME      :u32  = 0x00040000;
pub const WS_GROUP           :u32  = 0x00020000;
pub const WS_TABSTOP         :u32  = 0x00010000;
pub const WS_MINIMIZEBOX     :u32  = 0x00020000;
pub const WS_MAXIMIZEBOX     :u32  = 0x00010000;
pub const WS_OVERLAPPED_WINDOW :u32 = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;

pub const WM_NULL    : u32 = 0x0000;
pub const WM_CREATE  : u32 = 0x0001;
pub const WM_DESTROY : u32 = 0x0002;
pub const WM_MOVE    : u32 = 0x0003;
pub const WM_SIZE    : u32 = 0x0005;
pub const WM_PAINT :u32 = 0x000f;
pub const WM_CLOSE :u32 = 0x0010;

pub const WHITE_BRUSH         : i32 = 0;
pub const LTGRAY_BRUSH        : i32 = 1;
pub const GRAY_BRUSH          : i32 = 2;
pub const DKGRAY_BRUSH        : i32 = 3;
pub const BLACK_BRUSH         : i32 = 4;
pub const NULL_BRUSH          : i32 = 5;
pub const HOLLOW_BRUSH        : i32 = NULL_BRUSH;
pub const WHITE_PEN           : i32 = 6;
pub const BLACK_PEN           : i32 = 7;
pub const NULL_PEN            : i32 = 8;
pub const OEM_FIXED_FONT      : i32 = 10;
pub const ANSI_FIXED_FONT     : i32 = 11;
pub const ANSI_VAR_FONT       : i32 = 12;
pub const SYSTEM_FONT         : i32 = 13;
pub const DEVICE_DEFAULT_FONT : i32 = 14;
pub const DEFAULT_PALETTE     : i32 = 15;
pub const SYSTEM_FIXED_FONT   : i32 = 16;


pub const CS_VREDRAW          :u32 = 0x0001;
pub const CS_HREDRAW          :u32 = 0x0002;
pub const CS_DBLCLKS          :u32 = 0x0008;
pub const CS_OWNDC            :u32 = 0x0020;
pub const CS_CLASSDC          :u32 = 0x0040;
pub const CS_PARENTDC         :u32 = 0x0080;
pub const CS_NOCLOSE          :u32 = 0x0200;
pub const CS_SAVEBITS         :u32 = 0x0800;
pub const CS_BYTEALIGNCLIENT  :u32 = 0x1000;
pub const CS_BYTEALIGNWINDOW  :u32 = 0x2000;
pub const CS_GLOBALCLASS      :u32 = 0x4000;

pub const SW_SHOW : CInt = 5;


#[allow(overflowing_literals)]
pub const CW_USEDEFAULT :i32 = 0x80000000;

#[repr(C)]
pub struct WNDCLASSEXA {
    pub cbSize:UINT,
    /* Win 3.x */
    pub style: UINT,
    pub lpfnWndProc : WNDPROC,
    pub cbClsExtra : CInt,
    pub cbWndExtra : CInt,
    pub hInstance : HINSTANCE,
    pub hIcon : HICON,
    pub hCursor : HCURSOR,
    pub hbrBackGround : HBRUSH,
    pub lpszMenuName : LPCSTR,
    pub lpszClassName : LPCSTR,
    /* Win 4.0 */
    pub hIconSm : HICON
}

#[repr(C)]
pub struct POINT {
    x : LONG,
    y : LONG
}

#[repr(C)]
pub struct MSG {
    hwnd : HWND,
    message : UINT,
    wParam : WPARAM,
    lParam : LPARAM,
    time : DWORD,
    pt : POINT
}

#[repr(C)]
pub struct RECT {
    left: LONG,
    top : LONG,
    right : LONG,
    bottom : LONG
}

#[repr(C)]
pub struct PAINTSTRUCT {
    hdc : HDC,
    fErase : BOOL,
    rcPaint : RECT,
    fRestore : BOOL,
    fInclhUpdate : BOOL,
    rgbReserved : [u8; 32]
}

pub const IDC_ARROW : IntPtr = 32512;
pub const IDC_MAINMENU : IntPtr = 32512;

#[link(name="gdi32")]
extern "stdcall" {
    pub fn MessageBoxA(hWnd: HWND, lpText:*const CChar, lpCaption:*const CChar, uType: CUInt) -> CInt;
    pub fn DebugBreak();
    pub fn LoadLibraryA(_ : * const CChar) -> HMODULE;
    pub fn GetProcAddress(_ : HMODULE, _ : * const CChar) -> HMODULE;
    pub fn DefWindowProcA(hWnd:HWND, msg:UINT, wparam:WPARAM, lparam:LPARAM) -> LRESULT;
    pub fn PostQuitMessage(_:CInt);
    pub fn GetModuleHandleA(_ : LPCSTR) -> HMODULE;
    pub fn LoadCursorA(_:HINSTANCE, _:LPCSTR) -> HCURSOR;
    pub fn RegisterClassExA( _ : * const WNDCLASSEXA);
    pub fn CreateWindowExA( dwExStyle : DWORD,
                            lpClassName : LPCSTR,
                            lpWindowName : LPCSTR,
                            dwStyle : DWORD,
                            X : CInt,
                            Y : CInt,
                            nWidth : CInt,
                            nHeight : CInt,
                            hWndParent : HWND,
                            hMenu : HMENU,
                            hInstance : HINSTANCE,
                            lpParam : LPVOID) -> HWND;
    pub fn ShowWindow(w:HWND, sw:CInt) -> BOOL;
    pub fn UpdateWindow(w:HWND) -> BOOL;
    pub fn GetMessageA( _:*mut MSG, w:HWND, min:UINT, max:UINT) -> DWORD;
    pub fn TranslateMessage( _:*const MSG)->BOOL;
    pub fn DispatchMessageA( _:*const MSG)->LRESULT;
    pub fn BeginPaint(_:HWND, _:*mut PAINTSTRUCT) -> HDC;
    pub fn EndPaint(_:HWND, _:*const PAINTSTRUCT);
    pub fn GetStockObject(i : CInt) -> HGDIOBJ;
    pub fn SelectObject(dc : HDC, h: HGDIOBJ) -> HGDIOBJ;
    pub fn Rectangle(dc : HDC, left: CInt, top: CInt, right: CInt, bottom : CInt);
    pub fn TextOutA(dc : HDC, x : CInt, y: CInt, str:LPCSTR, len:CInt);
}
